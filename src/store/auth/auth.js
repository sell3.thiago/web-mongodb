import axios from 'axios'

export default {
    namespaced: true,
    state: {
        token: null,
        user: null,
        authenticated: false
    },
    getters: {
        getToken(state) {
            return state.token
        },
        authenticated(state) {
            return state.authenticated;
        },
        user(state) {
            return state.user
        }
    },
    mutations: {
        SET_TOKEN(state, token) {
            state.token = token
        },
        SET_USER(state, data) {
            state.user = data
        },
        SET_AUTHENTICATED(state, data) {
            state.authenticated = data
        },

    },
    actions: {

        async signIn({ dispatch }, credentials) {
            const { data } = await axios.post('/auth', credentials)
            return dispatch('attempt', data.data)
        },
        setTokenUser({ commit }, data) {
            commit('SET_TOKEN', data.token)
            commit('SET_USER', data.user)
            commit('SET_AUTHENTICATED', true)
        },

        async attempt({ commit, state }, data) {
            if (data) {
                commit('SET_TOKEN', data.access_token)
                axios.defaults.headers.common['auth-token'] = data.access_token
                sessionStorage.setItem('token', data.access_token);
                sessionStorage.setItem('user', JSON.stringify(data.user));


                commit('SET_USER', data.user)
                commit('SET_AUTHENTICATED', true)
            }
            if (!state.token) {
                return
            }
        },
        async signOut({ commit }) {
            return axios.post('auth/logout')
                .then(() => {
                    commit('SET_USER', null)
                    commit('SET_TOKEN', null)
                })
        },
    }
}