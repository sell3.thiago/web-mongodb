import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

import store from '@/store/index';

Vue.use(VueRouter)

const routes = [
  {
    path: "*",
    name: 'home',
    component: Home,
  },
  {
    path: '/login',
    name: 'login',
    component: () => import("@/components/auth/Login.vue"),
  },
  {
    path: '/home',
    name: 'home',
    component: Home,
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})


export default router
