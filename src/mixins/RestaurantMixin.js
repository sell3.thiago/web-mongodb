export default {
    props: {
        image: {
            type: String,
            default: () => ''
        },
        restaurant: {
            type: String,
        },
        email: {
            type: String,
            default: () => ''
        },
        instagram: {
            type: String,
            default: () => ''
        },
        descripcion: {
            type: String,
            default: () => ''
        },
        address: {
            type: String,
            default: () => ''
        },
        phone: {
            type: String,
            default: () => ''
        },
        rating: {
            type: Number,
            default: () => 0
        },
        typeFood: {
            type: Array,
            default: () => []
        },
        id: {
            type: String
        }
    }
}