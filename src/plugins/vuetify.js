import Vue from 'vue';
import Vuetify from 'vuetify/lib/framework';
import 'material-design-icons-iconfont/dist/material-design-icons.css'
import '@mdi/font/css/materialdesignicons.css'

import colors from 'vuetify/lib/util/colors'

Vue.use(Vuetify);

export default new Vuetify({
    theme: {
        dark: false,
        themes: {
            light: {
                primary: {
                    base: colors.blue.base,
                },
                primary2: {
                    base: colors.blue.darken2
                },
                secondary: {
                    base: colors.grey.base
                },
            },
            // Colores para tema dark
            dark: {
                primary2: {
                    base: colors.blue.darken2
                },
                secondary: {
                    base: colors.grey.base
                },
            },
        },
    },
    icons: {
        iconfont: 'mdi', // 'mdi' || 'mdiSvg' || 'md' || 'fa' || 'fa4' || 'faSvg'
    },
});
