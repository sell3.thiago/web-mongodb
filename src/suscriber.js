import store from '@/store/index.js'
import axios from 'axios'

import { mapActions } from 'vuex'
store.subscribe((mutation) => {
    switch (mutation.type) {
        case 'auth/SET_TOKEN':
            if (mutation.payload) {
                axios.defaults.headers.common['auth-token'] = mutation.payload
            } else {
                axios.defaults.headers.common['auth-token'] = null
                sessionStorage.removeItem('token')
                sessionStorage.removeItem('user')

                mapActions(['signOut'])
            }
            break;
    }
})

